#include <stdio.h>

char *human_readable_time (unsigned seconds, char *time_string)
{
	*time_string = '\0'; // write to time_string
  
  int remaining = seconds;
  int h = remaining / (60*60);
  remaining = remaining % (60*60);
  int m = remaining / 60;
  int s = remaining % 60;

  asprintf(&time_string, "%02d:%02d:%02d", h, m, s);
  
	return time_string; // return it
}